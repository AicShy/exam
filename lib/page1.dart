import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:encrypt/encrypt.dart' as Enc;
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'page2.dart';

String generateKey(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

class Page1 extends StatefulWidget {
  Page1({Key? key}) : super(key: key);
  @override
  _Page1State createState() => _Page1State();
}

class _Page1State extends State<Page1> {
  final passField = TextEditingController();

  @override
  void dispose() {
    passField.dispose();
    super.dispose();
  }

  late String _dec;
  late String decrypted;

  Future<String> decrypt(String keyStr) async {
    final cipherText = await read();
    if (cipherText != 'None') {
      keyStr = generateKey(keyStr);

      print("Ключ: $keyStr");
      final key = Enc.Key.fromUtf8(keyStr);
      final iv = Enc.IV.fromLength(16);

      final encrypter =
          Enc.Encrypter(Enc.AES(key, padding: null, mode: Enc.AESMode.cbc));
      final encryptedText = Enc.Encrypted.fromBase64(cipherText);

      final decrypted = encrypter.decrypt(encryptedText, iv: iv).toString();

      print("Расшифрованное: $decrypted");
      return decrypted;
    } else {
      print("Couldn't read file");
      return "Ошибка!";
    }
  }

  Future<String> read() async {
    try {
      print(rootBundle.loadString('assets/text.txt'));
      return await rootBundle.loadString('assets/text.txt');
    } catch (e) {
      return 'None';
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.grey[50],
      appBar: AppBar(
        title: Text("Войдите"),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 12, right: 12, top: 12, bottom: 12),
                  child: Container(
                    height: 60,
                    child: TextField(
                        controller: passField,
                        decoration: InputDecoration(
                          labelText: 'Введите пароль',
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: TextButton(
                    onPressed: () async {
                      String dec = await decrypt(passField.text);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Page2(content: dec)));
                    },
                    child: Text(
                      'Вход',
                      style: TextStyle(
                          color: Colors.black87, fontWeight: FontWeight.w400),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.grey[100]),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
